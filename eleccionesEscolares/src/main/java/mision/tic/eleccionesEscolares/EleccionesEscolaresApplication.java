package mision.tic.eleccionesEscolares;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EleccionesEscolaresApplication {

	public static void main(String[] args) {
		SpringApplication.run(EleccionesEscolaresApplication.class, args);
	}

}
